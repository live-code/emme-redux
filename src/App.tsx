import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { CounterPage } from './pages/counter/CounterPage';
import { CatalogPage } from './pages/catalog/CatalogPage';
import { Navbar } from './core/components/Navbar';
import { HomePage } from './pages/home/HomePage';
import { Action, combineReducers, configureStore, ThunkAction } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { counterReducer } from './pages/counter/store/counter.reducer';
import { newsStore } from './pages/home/model/news.store';
import { catalogReducer } from './pages/catalog/store';


const rootReducer = combineReducers({
  theme: () => 'dark',
  news: newsStore.reducer,
  counter: counterReducer,
  catalog: catalogReducer,
})

export type RootState = ReturnType<typeof rootReducer>
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>;
export const store = configureStore({
  reducer: rootReducer
})

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Navbar />

        <Switch>
          <Route path="/" exact>
            <HomePage />
          </Route>
          <Route path="/counter" exact>
            <CounterPage />
          </Route>
          <Route path="/catalog">
            <CatalogPage />
          </Route>
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;

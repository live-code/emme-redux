import React from 'react';
import { addNews, deleteNews } from './model/news.store';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../App';

export const HomePage: React.FC = () => {
  const dispatch = useDispatch();
  const newsList = useSelector((state: RootState) => state.news);

  function onChange(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(addNews(e.currentTarget.value))
      e.currentTarget.value = '';
    }
  }

  return <div>
    Ci sono {newsList.length} news
    <hr/>
    <input type="text" onKeyPress={onChange} placeholder="News Title (press enter)"/>

    {
      newsList.map(news => {
        return <li key={news.id}>
          {news.title}
          <i className="fa fa-trash"
             onClick={() => dispatch(deleteNews(news.id))} />
        </li>
      })
    }
  </div>
}

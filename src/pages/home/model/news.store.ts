import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { News } from './news';

export const newsStore = createSlice({
  name: 'news',
  initialState: [] as News[],
  reducers: {
    addNews(state, action: PayloadAction<string>) {
      const news: News = {
        id: Date.now(),
        title: action.payload,
        published: false
      }
      // IMMER JS REDUX TOOLKIT
      state.push(news);
      // Old School / IMMUTRABILITY
      // return [...state, news];
    },
    deleteNews(state, action: PayloadAction<number>) {
      const index = state.findIndex(news => news.id === action.payload);
      state.splice(index, 1);
    },
  }
})

export const { addNews, deleteNews }  = newsStore.actions

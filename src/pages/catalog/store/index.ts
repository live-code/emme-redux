import { combineReducers } from '@reduxjs/toolkit';
import { productsStore } from './products/products.store';
import { productsFiltersStore } from './filters/products-filter.store';

export const catalogReducer = combineReducers({
  list: productsStore.reducer,
  filters: productsFiltersStore.reducer,
})

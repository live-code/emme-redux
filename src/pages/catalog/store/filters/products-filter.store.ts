import { createSlice, PayloadAction } from '@reduxjs/toolkit'

export const productsFiltersStore = createSlice({
  name: 'products',
  initialState: {
    price: 0,
    visibility: true
  },
  reducers: {
    filterByPrice(state, action: PayloadAction<number>) {
      state.price = action.payload
    },

  }
});

export const {
  filterByPrice
} = productsFiltersStore.actions;

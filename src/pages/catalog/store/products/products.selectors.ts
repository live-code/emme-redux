import { RootState } from '../../../../App';

export const getProducts = (state: RootState) => state.catalog.list;
export const getFilters = (state: RootState) => state.catalog.filters;

export const getProductsFiltersByPrice =
  (state: RootState) => state.catalog.list.filter(p => p.price >= state.catalog.filters.price);

export const getTotal = (state: RootState) => state.catalog.list.reduce((acc, curr) => acc + curr.price, 0)

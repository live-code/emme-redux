// THUNK

import { AppThunk } from '../../../../App';
import axios from 'axios';
import { addProductSuccess, deleteProductSuccess, getProductsSuccess } from './products.store';
import { Product } from '../../model/product';
import { createAction } from '@reduxjs/toolkit';

export const getProducts = (): AppThunk => async dispatch => {
  try {
    const response = await axios.get<Product[]>('http://localhost:3001/products')
    dispatch(getProductsSuccess(response.data))
  } catch(err) {
    // dispatch error ....
  }
}

export const deleteProduct = (id: number): AppThunk => async dispatch => {
  try {
    await axios.delete(`http://localhost:3001/products/${id}`);
    dispatch(deleteProductSuccess(id))
  } catch (err) {
    // dispatch error
  }
};

export const addProduct = (
  product: Pick<Product, 'title' | 'price'>
): AppThunk => async dispatch => {

  const payload = { ...product, visibility: false }
  dispatch(addProductInit(payload));
  try {
    const newProduct = await axios.post<Product>('http://localhost:3001/products', payload);
    dispatch(addProductSuccess(newProduct.data))
  } catch (err) {
    dispatch(addProductFailed())
  }
};

export const addProductInit = createAction<Pick<Product, 'title' | 'price'>>('addProduct')
export const addProductFailed = createAction('addProductFailed')

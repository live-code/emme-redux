import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addProduct, getProducts } from './store/products/products.actions';
import { RootState } from '../../App';
import { filterByPrice } from './store/filters/products-filter.store';
import { getFilters, getProductsFiltersByPrice, getTotal } from './store/products/products.selectors';

export const CatalogPage: React.FC = () => {
  const dispatch = useDispatch();
  const products = useSelector(getProductsFiltersByPrice)
  const filters = useSelector(getFilters);
  const total = useSelector(getTotal)

  useEffect(() => {
     dispatch(getProducts())
  }, [])

  function addTodoHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(addProduct({
        title: e.currentTarget.value,
        price: 10,
      }))
      e.currentTarget.value = '';
    }
  }


  return <div>
    CatalogPage: {products.length} prodotti
    <hr/>
    <input type="text" onKeyPress={addTodoHandler}/>

    {
      products.map(p => {
        return <li key={p.id}>{p.title} - {p.price}</li>
      })
    }

    <h1>TOTAL: { total }</h1>
    <button
      style={{ color: filters.price === 5 ? 'red' : 'black'}}
      onClick={() => dispatch(filterByPrice(5))}>5</button>
    <button
      style={{ color: filters.price === 10 ? 'red' : 'black'}}
      onClick={() => dispatch(filterByPrice(10))}>10</button>

  </div>
};

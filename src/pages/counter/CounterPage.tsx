import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { decrement, increment } from './store/counter.actions';
import { selectCounter } from './store/counter.selectors';

export const CounterPage: React.FC = () => {
  const counter = useSelector(selectCounter)
  const dispatch = useDispatch();

  return <div>
    <h1>Counter {counter}</h1>

    <button onClick={() => dispatch(increment(10))}>+10</button>
    <button onClick={() => dispatch(decrement(5))}>-5</button>
  </div>
};

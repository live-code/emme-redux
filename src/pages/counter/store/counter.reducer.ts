import { createReducer, PayloadAction } from '@reduxjs/toolkit';
import { decrement, increment } from './counter.actions';

/*
export const counterReducer = createReducer(100, {
  [increment.type]: (state, action: PayloadAction<number>) => state + action.payload,
  [decrement.type]: (state, action: PayloadAction<number>) => state - action.payload,
})
*/

export const counterReducer = createReducer(100, builder => {
  builder
    .addCase(increment, (state, action) => state + action.payload)
    .addCase(decrement, (state, action) => state - action.payload)
})
